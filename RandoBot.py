import mastermind
from random import *

class RandoBot:
    def __init__(self,codelength,numberOfColors):
        self.codelength = codelength
        self.turnsPlayed = list()
        self.rewards = list()
        self.numberOfColors = numberOfColors
        self.unplayed = list(range(numberOfColors))


    def getName(self):
        return "RandoBot"

    def giveRewards(self,rewards):
        self.rewards.append(rewards)

    def resetBot(self):
        self.turnsPlayed.clear()
        self.rewards.clear()

    def playMove(self):
        choices = [choice(self.unplayed) for x in range(self.codelength)]
        return choices


if __name__ == "__main__":
    gamesToPlay = 1000
    colors = 6
    size = 4
    if (len(sys.argv) == 4):
        gamesToPlay = int(sys.argv[1])
        colors = int(sys.argv[2])
        size = int(sys.argv[3])
    mm = mastermind.Mastermind(colors,size)
    codelen = mm.getCodelength()
    clrs = mm.getNumberOfColors()
    bot = RandoBot(codelen,clrs)
    cost = 0
    playedGames = 0
    while playedGames < gamesToPlay:
        for x in range(10):
            guesses = bot.playMove()
            white_pins,red_pins = mm.play_turn(guesses)
            if red_pins == codelen:
                break
        cost += (x+1)
        playedGames +=1
        if playedGames % 100 == 0:
            print("Simulated",playedGames,"games")
    averageCost = cost / playedGames 
    print("Average cost per game is:",averageCost)