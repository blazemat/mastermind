from torch import nn
import torch 
import gym
from collections import deque
import itertools
import numpy as np
import random
import mastermind
import DeepQPlusAgent
import matplotlib.pyplot as plt

GAMMA = 0.99
BATCH_SIZE = 64
BUFFER_SIZE = 50000
MIN_REPLAY_SIZE = 1000
EPSILON_START = 1.0
EPSILON_END = 0.02
EPSILON_DECAY = 10000
TARGET_UPDATE_FREQ = 1000
LR = 5e-4

gamesToPlay = 250000
print("Going to play",gamesToPlay,"games")
score,scores = [],[]
x_axis = []
env = mastermind.Mastermind(6,4)
agent = DeepQPlusAgent.Agent(gamma = GAMMA, epsilon = EPSILON_START, batch_size = BATCH_SIZE, n_actions = len(env.actionsAvailable), input_dims = [len(env.gameState)], lr = LR)
for game in range(gamesToPlay):
	turns = 0
	done = False
	observation = env.reset()
	#print(observation)
	while not done:
		action = agent.choose_action(observation)
		new_obs, reward, done = env.step(action)
		#print(new_obs)
		turns +=1
		agent.store_transition(observation,action,reward,new_obs,done)
		agent.learn()
		observation = new_obs
	score.append(turns)
	if (game)%1000 == 0 and game !=0:
		avg = np.mean(score[-1000:])
		x_axis.append(game)
		scores.append(avg)
		print("Games",game-1000,"to", game,"avg:",avg)
	

plt.plot(x_axis,scores)
plt.xlabel("Her odehrano")
plt.ylabel("Průměrně kol za posledních 1000 her")
plt.title("Visualizace dat")

plt.show()


print("No problemo")