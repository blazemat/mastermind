import mastermind
import itertools
from random import *
import sys
import RandoBot
import WikiRandomChoiceBot
import WikiChoosingBot
import KnuthBot

if __name__ == "__main__":
    gamesToPlay = 10000
    colors = 8
    size = 4
    outtext = ""
    if (len(sys.argv) == 4):
        gamesToPlay = int(sys.argv[1])
        colors = int(sys.argv[2])
        size = int(sys.argv[3])
    mm = mastermind.Mastermind(colors,size)
    bots = list()
    bots.append(RandoBot.RandoBot(size,colors))
    bots.append(WikiRandomChoiceBot.WikiRandomChoiceBot(size,colors))
    bots.append(WikiChoosingBot.WikiChoosingBot(size,colors))
    bots.append(KnuthBot.KnuthBot(size,colors))
    for bot in bots:
        cost = 0
        playedGames = 0
        while playedGames < gamesToPlay:
            for x in range(10):
                guesses = bot.playMove()
                white_pins,red_pins = mm.play_turn(guesses)
                bot.giveRewards([white_pins,red_pins])
                if red_pins == size:
                    break
            bot.resetBot()
            cost += (x+1)
            playedGames +=1
            if playedGames % 1 == 0:
                print("Simulated",playedGames,"games of", bot.getName())
        averageCost = cost / playedGames 
        print("Average cost per game of",bot.getName(), "is:",averageCost,"after",playedGames,"games")
        outtext += "Average cost per game of "+bot.getName()+ " is: "+str(averageCost)+" after "+str(playedGames)+" games\n"
    with open("results.txt",'w') as output:
        output.write(outtext)