from random import *
import numpy as np
import itertools

class Mastermind:
	def __init__(self,colors = 6,codelength = 4):
		self.colors = list(x for x in range(colors))
		self.turns = 10
		self.thisturn = 1
		self.codelength = codelength
		self.code = list()
		self.generateNewCode()
		self.gameState = np.zeros(len(self.colors) * (self.codelength) * self.turns + self.codelength * 2 * self.turns, dtype = np.float32)
		self.actionsAvailable = list(itertools.product(self.colors,repeat = self.codelength))
		self.gameOver = False
		#self.translateGamestate()

	def step(self,action):
		move = self.actionsAvailable[action]
		#print(move)
		done = False
		white, red = self.play_turn(move,reset = False)
		reward = -1
		if red == self.codelength:
			reward +=10
			done = True
		if self.gameOver:
			done = True
			self.gameOver =False
		new_obs = self.gameState

		return new_obs, reward, done





	def reset(self):
		self.generateNewCode()
		self.gameOver = False
		self.gameState = np.zeros(len(self.colors) * (self.codelength) * self.turns + self.codelength * 2 * self.turns, dtype = np.float32)
		return self.gameState

	def generateNewCode(self):
		self.code.clear()
		self.thisturn = 1
		#self.code = [0, 7, 5, 4]
		for x in range(self.codelength):
			self.code.append(randint(0,len(self.colors)-1))
		#print(self.code)

	def play_turn(self,played,reset = True):
		#print("Turn:",self.thisturn)
		correct_color = 0
		correct_placement = 0
		codecpy = self.code.copy()
		if len(played) != self.codelength:
			#print("Wrong length! Your list has to be",self.codelength,"long")
			return [0,0]
		else:
			for x in range(self.codelength):
				if self.is_inside(played[x]):
					if played[x] == codecpy[x]:
						correct_placement += 1
						codecpy[x] = -1
				else:
					return [0,0]
			for x in range(self.codelength):
				if played[x] == self.code[x]:
					continue
				if played[x] in codecpy:
					codecpy[codecpy.index(played[x])] = -1
					correct_color += 1
		self.captureGamestate(played,correct_placement,correct_color)
		if (correct_placement) == self.codelength:
			#print("You won in",self.thisturn,"turns, generating another code")
			self.thisturn = 1
			self.generateNewCode()
			return [correct_color, correct_placement]
		else:
			if self.turns == self.thisturn:
				#print("You could not win in",self.thisturn,"turns, generating another code")
				#print("Correct code was:",self.code)
				self.thisturn = 0
				if reset:
					self.generateNewCode()
				self.gameOver = True
			self.thisturn +=1
			return [correct_color, correct_placement]
	
	def captureGamestate(self,played,red,white):
		played = list(played)
		turnlen = len(self.colors) * self.codelength + self.codelength * 2
		for index,element in enumerate(played):
			self.gameState[(self.thisturn-1)*turnlen + index * len(self.colors) + element] = np.float32(1)
		if white > 0:
			self.gameState[(self.thisturn-1) * turnlen + len(self.colors) * self.codelength + white-1] = np.float32(1)
		if red > 0:
			self.gameState[(self.thisturn-1) * turnlen + len(self.colors) * self.codelength + self.codelength + red-1] = np.float32(1)
		#print(self.gameState)




	def is_inside(self,number):
		if isinstance(number,int) and number < len(self.colors) and number >= 0:
			return True
		print("Not enough ball colours or not int")
		return False

	def getCodelength(self):
		return self.codelength



	def getNumberOfColors(self):
		return len(self.colors)

if __name__ == "__main__":
	mastermind = Mastermind()
	while True:
		guesses = list(map(int,input().split()))
		white_pins,red_pins = mastermind.play_turn(guesses)
		print(white_pins,red_pins)

