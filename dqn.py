from torch import nn
import os
import torch
import gym
from collections import deque
import itertools
import numpy as np
import random
from mastermind import Mastermind as mastermind
import msgpack
from msgpack_numpy import patch as msgpack_numpy_patch

GAMMA=0.99
BATCH_SIZE=32
BUFFER_SIZE=50000
MIN_REPLAY_SIZE=1000
EPSILON_START=1.0
EPSILON_END=0.02
EPSILON_DECAY=10000
TARGET_UPDATE_FREQ = 1000
SAVE_PATH = "./mastermind_dqn.pack"
SAVE_INTERVAL = 10000



class Network(nn.Module):
    def __init__(self, env):
        super().__init__()

        in_features = int(np.prod(env.gameState.shape))

        self.net = nn.Sequential(
            nn.Linear(in_features, 256),
            nn.Tanh(),
            nn.Linear(256, len(env.actionsAvailable))
        )

    def forward(self, x):
        return self.net(x)

    def act(self, obs):
        obs_t = torch.as_tensor(obs, dtype=torch.float32)
        q_values = self(obs_t.unsqueeze(0))
        max_q_index = torch.argmax(q_values, dim=1)[0]
        action = max_q_index.detach().item()

        return action

    def save(self, save_path):
        parameters = {k: t.detach().cpu().numpy() for k,t in self.state_dict().items()}
        p_data = msgpack.dumps(parameters)

        os.makedirs(os.path.dirname(save_path), exist_ok = True)
        with open(save_path,"wb") as f:
            f.write(p_data)

    def load(self,load_path):
        if not os.path.exists(load_path):
            return

        with open(load_path, "rb") as f:
            params_numpy = msgpack.loads(f.read())
        parameters = {k: torch.as_tensor(v) for k,v in paramets_numpy.items()}

        self.load_state_dict(parameters)

    def calculate_loss(self,transitions,target_net):
        obses = np.asarray([t[0] for t in transitions])
        actions = np.asarray([t[1] for t in transitions])
        rews = np.asarray([t[2] for t in transitions])
        dones = np.asarray([t[3] for t in transitions])
        new_obses = np.asarray([t[4] for t in transitions])

        obses_t = torch.as_tensor(obses, dtype=torch.float32)
        actions_t = torch.as_tensor(actions, dtype=torch.int64).unsqueeze(-1)
        rews_t = torch.as_tensor(rews, dtype=torch.float32).unsqueeze(-1)
        dones_t = torch.as_tensor(dones, dtype=torch.float32).unsqueeze(-1)
        new_obses_t = torch.as_tensor(new_obses, dtype=torch.float32)

        # Compute Targets
        target_q_values = target_net(new_obses_t)
        max_target_q_values = target_q_values.max(dim=1, keepdim=True)[0]

        targets = rews_t + GAMMA * (1 - dones_t) * max_target_q_values

        # Compute Loss
        q_values = online_net(obses_t)
        action_q_values = torch.gather(input=q_values, dim=1, index=actions_t)

        loss = nn.functional.smooth_l1_loss(action_q_values, targets)

        return loss


env = mastermind(6,4)
replay_buffer = deque(maxlen=BUFFER_SIZE)
rew_buffer = deque([0.0], maxlen=100)
turn_buffer = deque(maxlen = 100)

episode_reward = 0
turns = 0


online_net = Network(env)
target_net = Network(env)

target_net.load_state_dict(online_net.state_dict())

optimizer = torch.optim.Adam(online_net.parameters(), lr=5e-4)

# Initialize replay buffer
obs = env.reset()
for _ in range(MIN_REPLAY_SIZE):
    action = random.randrange(0,len(env.actionsAvailable))

    new_obs, rew, done = env.step(action)
    transition = (obs, action, rew, done, new_obs)
    replay_buffer.append(transition)
    obs = new_obs

    if done:
        obs = env.reset()


# Main Training Loop
obs = env.reset()
for step in itertools.count():
    epsilon = np.interp(step, [0, EPSILON_DECAY], [EPSILON_START, EPSILON_END])

    rnd_sample = random.random()
    if rnd_sample <= epsilon:
        action = random.randrange(0,len(env.actionsAvailable))
    else:
        action = online_net.act(obs)

    new_obs, rew, done = env.step(action)
    transition = (obs, action, rew, done, new_obs)
    replay_buffer.append(transition)
    obs = new_obs

    episode_reward += rew
    turns += 1

    if done:
        obs = env.reset()
        turn_buffer.append(turns)
        turns = 0

        rew_buffer.append(episode_reward)
        episode_reward = 0

    if len(rew_buffer) >= 100:
        if np.mean(rew_buffer) >= 195:
            while True:
                action = online_net.act(obs)

                obs, _, done, _ = env.step(action)
                env.render()
                if done: 
                    env.reset()

    transitions = random.sample(replay_buffer, BATCH_SIZE)

    loss = online_net.calculate_loss(transitions,target_net)


    optimizer.zero_grad()
    loss.backward()
    optimizer.step()


    if step % TARGET_UPDATE_FREQ == 0:
        target_net.load_state_dict(online_net.state_dict())

    # Logging
    if step % 1000 == 0:
        print()
        print('Step:', step)
        print('Avg Rew:', np.mean(rew_buffer))
        print("Took turns", np.mean(turn_buffer))

    if step%SAVE_INTERVAL == 0 and step != 0:
        print("Saving")
        online_net.save(SAVE_PATH)










