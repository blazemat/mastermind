import mastermind
import itertools
from random import *
import sys

class WikiRandomChoiceBot:
    def __init__(self,codelength,numberOfColors):
        self.codelength = codelength
        self.turnsPlayed = list()
        self.rewards = list()
        self.numberOfColors = numberOfColors
        self.unplayed = list(range(numberOfColors))
        self.possibleCodes = list(itertools.product(self.unplayed,repeat = self.codelength))

    def getName(self):
        return "WikiRandomChoiceBot"

    def resetBot(self):
        self.turnsPlayed.clear()
        self.rewards.clear()
        self.possibleCodes = list(itertools.product(self.unplayed,repeat = self.codelength))

    def giveRewards(self,rewards):
        self.rewards.append(rewards)

    def playMove(self):
        if len(self.turnsPlayed) == 0:
            ret =[0,0]
            for x in range(self.codelength - len(ret)):
                ret.append(1)
            self.turnsPlayed.append(ret)
            return ret
        else:
            last_move = self.turnsPlayed[-1]
            white_pins = self.rewards[-1][0]
            red_pins = self.rewards[-1][1]
            new_moves = list()
            for move in self.possibleCodes:
                if (self.compare_to_played(last_move,move,red_pins,white_pins)):
                    new_moves.append(move)
                    #print(move)
            #print(len(self.possibleCodes))
            self.possibleCodes = new_moves.copy()
            #print(len(self.possibleCodes))
        move = self.get_best_move()
        self.turnsPlayed.append(move)

        
        #print(move)
        return move
    
    def get_best_move(self):
        return choice(self.possibleCodes)

    def compare_to_played(self,played,compared,red_pins,white_pins):
        deepcpy = list(played).copy()
        compared = list(compared)
        red_guesses = 0
        white_guesses = 0
        for ind,color in enumerate(compared):
            if color == played[ind]:
                red_guesses +=1
                deepcpy[ind] = -1
                compared[ind] = -2
        for ind,color in enumerate(compared):
            if color in deepcpy:
                white_guesses +=1
                deepcpy.remove(color)
        return white_guesses == white_pins and red_guesses == red_pins




if __name__ == "__main__": 
    gamesToPlay = 1000
    colors = 8
    size = 4
    if (len(sys.argv) == 4):
        gamesToPlay = int(sys.argv[1])
        colors = int(sys.argv[2])
        size = int(sys.argv[3])
    mm = mastermind.Mastermind(colors,size)
    codelen = mm.getCodelength()
    clrs = mm.getNumberOfColors()
    bot = WikiRandomChoiceBot(codelen,clrs)
    cost = 0
    playedGames = 0
    while playedGames < gamesToPlay:
        for x in range(10):
            guesses = bot.playMove()
            white_pins,red_pins = mm.play_turn(guesses)
            #print(white_pins,red_pins)
            bot.giveRewards([white_pins,red_pins])
            if red_pins == size:
                bot.resetBot()
                break
        cost += (x+1)
        playedGames +=1
        if playedGames % 1 == 0:
            print("Simulated",playedGames,"games")
    averageCost = cost / playedGames 
    print("Average cost per game is:",averageCost)