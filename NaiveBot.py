import mastermind
import itertools
from random import *

class Guess:
    def __init__(self,sequence,unplayed):
        self.code = sequence
        self.unplayed = unplayed
    
    def filled(self):
        filled_code = self.code.copy()
        for index,element in enumerate(self.code):
            if element == -1:
                filled_code[index] = self.unplayed[0]
        return filled_code



class NaiveBot:
    def __init__(self,codelength,numberOfColors):
        self.codelength = codelength
        self.turnsPlayed = list()
        self.rewards = list()
        self.numberOfColors = numberOfColors
        self.unplayed = list(range(numberOfColors))
        self.guesses = list()


    def resetBot(self):
        self.turnsPlayed.clear()
        self.rewards.clear()
        self.unplayed = list(range(self.numberOfColors))
        self.guesses.clear()

    def playFirstTurn(self):
        ret =[0,0]
        for x in range(self.codelength - len(ret)):
            ret.append(1)
        return ret

    def endTurn(self,answer):
        print("Played",answer)
        self.turnsPlayed.append(answer)
        for element in answer:
            if element in self.unplayed:
                self.unplayed.remove(element)
        

    def playMove(self):
        answer = [0,0,0,0]
        if len(self.turnsPlayed) == 0:
            answer =  self.playFirstTurn()
        elif len(self.guesses) == 0 or self.isFullOf([0,0],self.rewards):
            red_pins = self.rewards[-1][1]
            white_pins = self.rewards[-1][0]
            if red_pins == 0 and white_pins == 0:
                answer = [self.unplayed[0],self.unplayed[0],self.unplayed[1],self.unplayed[1]]
            else:
                if red_pins > 0:
                    possible_red_locations = self.possibleRedPinsLists([-1 for x in range(4)],red_pins)
                    for guess in possible_red_locations:
                        possible_locations = self.possibleWhitePinsLists(guess,white_pins)
                        for loc in possible_locations:
                            self.guesses.append(Guess(loc,self.unplayed))
                    answer = self.guesses[0].filled()
        else:
            red_pins = self.rewards[-1][1]
            white_pins = self.rewards[-1][0]
            new_guesses = list()
            for guess in self.guesses:
                code = guess.code
                matches,colormatches = self.find_matches_and_color(code,self.turnsPlayed[-1])
                if matches > red_pins:
                    continue
                #if matches + colormatches > red_pins + white_pins:
                #    print("Skipped",code)
                #    continue
                #print("Original:",code)
                codes =self.eliminationLogik(white_pins,red_pins,code,matches,colormatches)
                for code2 in codes:
                    new_guesses.append(Guess(code2,self.unplayed))
            self.guesses.clear()
            self.guesses= new_guesses.copy()
            answer = self.guesses[0].filled()


        self.endTurn(answer)
        return answer

    def eliminationLogik(self,white_pins,red_pins,code,matches,colormatches):
        answer =list()
        indexes = self.findIndexes(code)
        played = self.playedNumbers(code)
        possibilities = self.possibleWhitePinsLists(code,white_pins)
        for pos in possibilities:
            if(not self.isInPlayed(pos,played,indexes)):
                print(pos)
                answer.append(pos)
        if matches < red_pins:
            possibilities = self.possibleRedPinsLists(code,red_pins-matches)
            #print(code,possibilities)
            for pos in possibilities:
                if(not self.isInPlayed(pos,played,indexes)):
                    print("Pridan natvrdo:",pos)
                    answer.append(pos)
        return answer

        
                
    def isInPlayed(self,code,played,indexes):
        ret = False
        for index in indexes:
            if code[index] in played:
                ret = True
                break
        return ret


    def playedNumbers(self,code):
        answer = list()
        for element in code:
            if element != -1:
                answer.append(element)
        return answer

    def giveRewards(self,rewards):
        self.rewards.append(rewards)

    def find_matches_and_color(self,list,list2):
        matches = 0
        color = 0
        cpy = list.copy()
        for x in range(len(list)):
            if list[x] == list2[x]:
                matches +=1
                cpy[x] = -1
        for element in cpy:
            if element in list2:
                color +=1
        return matches,color

    def findIndexes(self,initial_list):
        indexes = list()  
        for index,element in enumerate(initial_list):
            if element == -1:
                indexes.append(index)
        return indexes

    def possibleWhitePinsLists(self,initial_list,white_pins):
        answer = list()
        indexes = self.findIndexes(initial_list)
        combinations = list(itertools.combinations(indexes,white_pins))
        for combination in combinations:
            guessCopiesNow = list()
            guessCopiesNext = list()
            guessCopiesNow.append(initial_list.copy())
            for index in combination:
                for copy in guessCopiesNow:
                    for x in range(self.codelength):
                        if self.turnsPlayed[-1][x] != self.turnsPlayed[-1][index] and copy[x] == -1:
                            newGuess = copy.copy()
                            newGuess[x] = self.turnsPlayed[-1][index]
                            guessCopiesNext.append(newGuess)
                guessCopiesNow = guessCopiesNext.copy()
                guessCopiesNext.clear()
            for theRealOne in guessCopiesNow:
                if theRealOne not in answer:
                    answer.append(theRealOne)
                    #print(theRealOne)

        return answer



    def possibleRedPinsLists(self,initial_list,red_pins):
        answer = list()
        indexes = self.findIndexes(initial_list)
        combinations = list(itertools.combinations(indexes,red_pins))
        for combination in combinations:
            current_guess = initial_list.copy()
            for index in combination:
                current_guess[index] = self.turnsPlayed[-1][index]
            answer.append(current_guess)
        #print(answer)
        return answer


        

    def isFullOf(self,compared,lis):
        ret = True
        for element in lis:
            if element != compared:
                ret = False
                break
                
        return ret



if __name__ == "__main__":
    mm = mastermind.Mastermind()
    codelen = mm.getCodelength()
    clrs = mm.getNumberOfColors()
    bot = NaiveBot(codelen,clrs)
    cost = 0
    playedGames = 0
    while playedGames < 1000:
        for x in range(10):
            guesses = bot.playMove()
            white_pins,red_pins = mm.play_turn(guesses)
            print(white_pins,red_pins)
            bot.giveRewards([white_pins,red_pins])
            if red_pins == 4:
                break
        cost += (x+1)
        playedGames +=1
        bot.resetBot()
        if playedGames % 100 == 0:
            print("Simulated",playedGames,"games")
    averageCost = cost / playedGames 
    print("Average cost per game is:",averageCost)
