import mastermind
import itertools
from random import *
import sys
from timeit import default_timer as timer

class KnuthBot:
    def __init__(self,codelength,numberOfColors):
        self.codelength = codelength
        self.turnsPlayed = list()
        self.rewards = list()
        self.numberOfColors = numberOfColors
        self.colors = list(range(numberOfColors))
        self.possibleCodes = list(itertools.product(self.colors,repeat = self.codelength))
        self.unplayedMoves = self.possibleCodes.copy()
        self.available_rewards = list(itertools.product(list(x for x in range(self.codelength+1)), repeat=2))


    def resetBot(self):
        self.turnsPlayed.clear()
        self.rewards.clear()
        self.possibleCodes = list(itertools.product(self.colors,repeat = self.codelength))
        self.unplayedMoves = self.possibleCodes.copy()

    def giveRewards(self,rewards):
        self.rewards.append(rewards)

    def getName(self):
        return "KnuthBot"

    def playMove(self):
        if len(self.turnsPlayed) == 0:
            ret =[0,0]
            for x in range(self.codelength - len(ret)):
                ret.append(1)
            self.turnsPlayed.append(ret)
            return ret
        else:
            last_move = self.turnsPlayed[-1]
            white_pins = self.rewards[-1][0]
            red_pins = self.rewards[-1][1]
            new_moves = list()
            for move in self.possibleCodes:
                if (self.compare_to_played(last_move,move,red_pins,white_pins)):
                    new_moves.append(move)
                    #print(move)
            #print(len(self.possibleCodes))
            self.possibleCodes = new_moves.copy()
            #print(len(self.possibleCodes))
        move = self.get_best_move()
        self.turnsPlayed.append(move)
        self.unplayedMoves.remove(move)

        
        #print(move)
        return move
    
    def get_best_move(self):
        removed = list()
        for index,code in enumerate(self.unplayedMoves):
           #print("trying",index,"code")
           score = [0 for x in range(len(self.available_rewards))]
           for index,pins in enumerate(self.available_rewards):
               white_pins,red_pins = pins
               for compared in self.possibleCodes:
                   if not self.compare_to_played(code,compared,red_pins,white_pins):
                       score[index] +=1
           #print(score)
           removed.append(min(score))
        correct_answer= self.find_correct_answer(removed)



        return correct_answer
    def find_correct_answer(self,scores):
        max = 0
        correct_answers = list()
        for index,score in enumerate(scores):
            if score == max:
                correct_answers.append(index)
            if score > max:
                max = score
                correct_answers.clear()
                correct_answers.append(index)
        for answer in correct_answers:
            if self.unplayedMoves[answer] in self.possibleCodes:
                return self.unplayedMoves[answer]
        return self.unplayedMoves[correct_answers[0]]
        
    def compare_to_played(self,played,compared,red_pins,white_pins):
        deepcpy = list(played).copy()
        compared = list(compared)
        red_guesses = 0
        white_guesses = 0
        for ind,color in enumerate(compared):
            if color == played[ind]:
                red_guesses +=1
                deepcpy[ind] = -1
                compared[ind] = -2
        for ind,color in enumerate(compared):
            if color in deepcpy:
                white_guesses +=1
                deepcpy.remove(color)
        return white_guesses == white_pins and red_guesses == red_pins





if __name__ == "__main__": 
    gamesToPlay = 100
    colors = 6
    size = 4
    if (len(sys.argv) == 4):
        gamesToPlay = argv[1]
        colors = argv[2]
        size = argv[3]
    mm = mastermind.Mastermind(colors,size)
    codelen = mm.getCodelength()
    clrs = mm.getNumberOfColors()
    bot = KnuthBot(codelen,clrs)
    cost = 0
    playedGames = 0
    start = timer()
# ...

    while playedGames < gamesToPlay:
        start = timer()
        for x in range(10):
            guesses = bot.playMove()
            white_pins,red_pins = mm.play_turn(guesses)
            #print(white_pins,red_pins)
            bot.giveRewards([white_pins,red_pins])
            if red_pins == size:
                bot.resetBot()
                break
        cost += (x+1)
        playedGames +=1
        end = timer()
        print("One game took",end - start)
        if playedGames % 1 == 0:
            print("Simulated",playedGames,"games")
    averageCost = cost / playedGames 
    print("Average cost per game is:",averageCost)